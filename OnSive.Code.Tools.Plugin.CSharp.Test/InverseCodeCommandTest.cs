using OnSive.Code.Tools.TestExtension;
using System;
using Xunit;

namespace OnSive.Code.Tools.Plugin.CSharp.Test
{
    public class InverseCodeCommandTest : ICommandTest<InverseCodeCommand>
    {
        [Fact]
        public async void RunTest()
        {
            InverseCodeCommand oCommand = new InverseCodeCommand();

            string sCodeOriginal = @"        public bool bFireClosingEvent { get; set; }

        public bool bChanged;

        public ChangelogEditor2(classExport.classProjekt_Changelog Changelog)
        {
            try
            {
                if (Changelog == null || Changelog == default(classExport.classProjekt_Changelog))
                {
                    throw new ArgumentNullException();
                }

                InitializeComponent();

                this.DataContext = Changelog;

                // Setzt den Button wieder auf standart
                labelDelete.Content = classTexte.labelDelete;
                labelID.Content = classTexte.ID;
                labelName.Content = classTexte.labelName;
                labelVersion.Content = classTexte.Version;

                TextBox_ID.Text = Changelog.Guid_Changelog.ToString();

                Build_Changelog();
            }
            catch (Exception ex)
            {
                // Bei einem Fehler wird die Error Funktion aufgerufen
                Proto.Error(ex);
            }
        }";

            string sCodeAfter = @"        public bool bFireClosingEvent { get; set; }

        public bool bChanged;

        public ChangelogEditor2(classExport.classProjekt_Changelog Changelog)
        {
            try
            {
                if (Changelog == null || Changelog == default(classExport.classProjekt_Changelog))
                {
                    throw new ArgumentNullException();
                }

                InitializeComponent();

Changelog = this.DataContext;

                // Setzt den Button wieder auf standart
classTexte.labelDelete = labelDelete.Content;
classTexte.ID = labelID.Content;
classTexte.labelName = labelName.Content;
classTexte.Version = labelVersion.Content;

Changelog.Guid_Changelog.ToString() = TextBox_ID.Text;

                Build_Changelog();
            }
            catch (Exception ex)
            {
                // Bei einem Fehler wird die Error Funktion aufgerufen
                Proto.Error(ex);
            }
        }";

            Assert.Equal(sCodeAfter, await oCommand.Run(sCodeOriginal));
        }
    }
}
