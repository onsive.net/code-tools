<br />
<div align="center">
  <img src="https://gitlab.com/onsive.net/Netflix.PersonalData/-/raw/master/OnSive.Netflix.PersonalData/Icon.png" alt="Logo" width="80" height="80">

  <h3 align="center">OnSive | Code Tool</h3>

  <p align="center">
    Small text refactoring software. All functions can be added as plugins.
  <br />
  <a href="/issues/new">Report Bug</a>
    ·
  <a href="/issues/new">Request Feature</a>
  </p>
</div>

### Usage

1. Download the exe and some plugins.
2. Run the exe.
3. Select in the combobox which plugin you would like to use.

### Write a plugin
1. Create a .NET 4.8 library and add `OnSive.Code.Tools.Common` as reference
2. Create a class and implement the `ICommand` interface.
4. You'll get the input as parameter of `Run` and can simply return the out text.
3. Compile the library and put it in the `\Plugins\` folder.

### WIP
- WebService for plugin upload and download

### Third Party

_Icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>_