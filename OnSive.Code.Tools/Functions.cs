﻿using OnSive.Code.Tools.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace OnSive.Code.Tools
{
    public static class Functions
    {
        public static IEnumerable<ICommand> LoadPlugin(FileInfo fiPlugin, out string sPluginName)
        {
            var dictCommands = new List<ICommand>();
            var oAssemblyPlugin = Assembly.LoadFile(fiPlugin.FullName);

            sPluginName = oAssemblyPlugin.GetName().Name.Replace("OnSive.Code.Tools.Plugin.", string.Empty);

            // Cannot directly instantiate an interface. Can only
            // instantiate classes. However, we don't know the name
            // of the class that supports this interface so we have
            // to brute force search for it.
            Type[] types = oAssemblyPlugin.GetTypes();
            foreach (Type type in types)
            {
                // Does this class support the transport interface?
                Type typeExample = type.GetInterface("ICommand");
                if (typeExample == null)
                {
                    // Not supported.
                    continue;
                }

                // This class supports the interface. Instantiate it.
                ICommand oCommand = oAssemblyPlugin.CreateInstance(type.FullName) as ICommand;
                if (oCommand != null)
                {
                    dictCommands.Add(oCommand);
                }
            }

            return dictCommands;
        }

        /// <summary>
        ///     Überprüft ob alle benötigten Referenzen vorhanden sind
        /// </summary>
        /// <param name="fiPlugin">FileInfo des Plugins</param>
        /// <param name="sPluginName">Der Name des Plugins</param>
        /// <param name="oCommand">Der Command des Plugins</param>
        /// <param name="dictMissingReferences">Das Verzeichnis mit allen fehlenden Referenzen</param>
        /// <returns>Ob alle Referenzen vorhanden sind</returns>
        public static bool CheckReferences(FileInfo fiPlugin, string sPluginName, ICommand oCommand, ref Dictionary<string, List<string>> dictMissingReferences)
        {
            bool bIsEnabled = true;
            foreach (var oReference in oCommand.References)
            {
                bool bIsEnabledTmp = true;
                if (File.Exists($"Plugins\\{oReference.AssemblyName}.dll"))
                {
                    var oAssemblyReference = Assembly.LoadFile(Path.Combine(fiPlugin.DirectoryName, $"{oReference.AssemblyName}.dll"));
                    if (oAssemblyReference.GetName().Version < oReference.MinVersion)
                    {
                        bIsEnabledTmp = false;
                    }
                }
                else
                {
                    bIsEnabledTmp = false;
                }

                if (!bIsEnabledTmp)
                {
                    dictMissingReferences[sPluginName].Add($" - [>={oReference.MinVersion}] {oReference.AssemblyName}");
                    bIsEnabled = false;
                }
            }

            return bIsEnabled;
        }

        public static string GenerateHash(string sInput)
        {
            using (MD5 md5 = MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(sInput)))
                                   .Replace("-", String.Empty);
            }
        }
    }
}
