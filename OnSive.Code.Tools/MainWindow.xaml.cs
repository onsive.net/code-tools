﻿using OnSive.Code.Tools.Common;
using OnSive.Code.Tools.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace OnSive.Code.Tools
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly Dictionary<string, List<Button>> dictButtons = new Dictionary<string, List<Button>>();

        readonly Dictionary<string, List<string>> dictMissingReferences = new Dictionary<string, List<string>>();

        public MainWindow()
        {
            try
            {
                InitializeComponent();

                Application.Current.MainWindow = this;

                var fiExe = new FileInfo(Assembly.GetExecutingAssembly().Location);

                foreach (var fiPlugin in fiExe.Directory.GetFiles("Plugins\\*.dll"))
                {
                    var Enumerable_Commands = Functions.LoadPlugin(fiPlugin, out string sPluginName);
                    sPluginName = BeautifyName(sPluginName);

                    dictButtons.Add(sPluginName, new List<Button>());
                    dictMissingReferences.Add(sPluginName, new List<string>());

                    foreach (var oCommand in Enumerable_Commands)
                    {
                        Button btn = new Button()
                        {
                            Content = oCommand.Title,
                            Tag = oCommand,
                            IsEnabled = Functions.CheckReferences(fiPlugin, sPluginName, oCommand, ref dictMissingReferences),
                            ToolTip = oCommand.Description,
                            Margin = new Thickness(0, 5, 0, 5)
                        };

                        btn.Click += async (sender, e) =>
                        {
                            var oCommandTmp = (ICommand)((Button)sender).Tag;
                            await Application.Current.MainWindow.Dispatcher
                                                                .Invoke(async () => TextEditor_CodeOut.Text = await oCommandTmp.Run(TextEditor_CodeIn.Text));
                        };

                        dictButtons[sPluginName].Add(btn);
                    }
                }

                foreach (var item in dictButtons.Keys)
                {
                    ComboBox_Plugin.Items.Add(item);
                }

                if (ComboBox_Plugin.Items.Count > 0)
                {
                    if (string.IsNullOrEmpty(Settings.Default.SelectedPlugin))
                    {
                        ComboBox_Plugin.SelectedIndex = 0;
                    }
                    else
                    {
                        ComboBox_Plugin.SelectedValue = Settings.Default.SelectedPlugin;
                    }
                }
            }
            catch (Exception ex)
            {
                OnError(ex);
            }
        }

        private static string BeautifyName(string sPluginName)
        {
            try
            {
                switch (sPluginName)
                {
                    case "CSharp":
                        sPluginName = "C#";
                        break;

                    case "VBNet":
                        sPluginName = "VB.NET";
                        break;

                    default:
                        break;
                }

                return sPluginName;
            }
            catch (Exception ex)
            {
                OnError(ex);
                return $"ERROR [{Guid.NewGuid()}]";
            }
        }

        public static T Instantiate<T>()
            where T : new()
        {
            return new T();
        }

        public static void OnError(Exception ex)
        {
            Application.Current.MainWindow.Dispatcher
                                          .Invoke(() => MessageBox.Show(ex.StackTrace, $"{Application.Current.MainWindow.Title} | Error: {ex.Message}"));
        }

        private void Button_CopyOutPut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Clipboard.SetText(TextEditor_CodeOut.Text);
            }
            catch (Exception ex)
            {
                OnError(ex);
            }
        }

        private void ComboBox_Plugin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                StackPanel_Buttons.Children.Clear();
                foreach (var btn in dictButtons[ComboBox_Plugin.SelectedValue.ToString()])
                {
                    StackPanel_Buttons.Children.Add(btn);
                }
                Settings.Default.SelectedPlugin = ComboBox_Plugin.SelectedValue.ToString();
                Settings.Default.Save();

                if (dictMissingReferences[ComboBox_Plugin.SelectedValue.ToString()].Count > 0)
                {
                    MessageBox.Show($"{$"The plugin is missing some references!{Environment.NewLine}{Environment.NewLine}"}{string.Join(Environment.NewLine, dictMissingReferences[ComboBox_Plugin.SelectedValue.ToString()])}",
                        "[OS] Code Tools | Missing References", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                OnError(ex);
            }
        }
    }
}
