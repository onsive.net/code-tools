using OnSive.Code.Tools.Common;
using OnSive.Code.Tools.TestExtension;
using System;
using Xunit;

namespace OnSive.Code.Tools.Plugin.VBNet.Test
{
    public class InverseCodeCommandTest : ICommandTest<InverseCodeCommand>
    {
        [Fact]
        public async void RunTest()
        {
            ICommand oCommand = new InverseCodeCommand();

            string sCodeOriginal = @"
    ''' <summary>
    ''' (De-)Aktiviert die Bearbeitungs-Felder, sowie das Gegenteil f�r alle davon abh�ngigen Controlls
    ''' </summary>
    ''' <param name=""bEnable"">Bearbeitungs-Modus aktivieren</param>
    Private Sub Enable_EditControls(bEnable As Boolean) Implements IAsyncFilterableEditForm.Enable_EditControls
        Try

            'Wenn kein g�ltiger Datensatz ausgew�hlt ist und 
            'die Bearbeitungs-Felder aktiviert werden sollen
            If iCurrentRecordID < 0 AndAlso bEnable Then
                'Abbruch, da sonst nur Fehler
                Exit Sub
            End If

            classFunktionen.Edit_Data(bEnable, LayoutControl_Main)

        Catch ex As Exception
            'Protokolliert den Fehler
            Proto.Error(ex)
            'Behandelt den Fehler
            OnError(ex)
        End Try
    End Sub

    Private Async Function LoadAsync(iRecordID As Integer) As Task(Of Integer) Implements IAsyncFilterableEditForm.LoadAsync
        Try
            'Wenn keine g�ltige RecordID �bergeben wurde
            If iRecordID < 0 Then
                'Abbrechen
                Return -1
            End If

            Dim oLost = Await SqlFactory.LoadAsync(Of LOST)(New Query(""BESCHWER"").Where(NameOf(LOST.RECORDID), iRecordID))

            'Daten einsetzten
            DateEdit_DateTime.EditValue = If(oLost.DATUM Is Nothing, Nothing, classFunktionen.ToDateTime(CType(oLost.DATUM, Date), oLost.UHRZEIT))
            TextEdit_Fundsache.EditValue = oLost.ITEM
            TextEdit_Ort.EditValue = oLost.FUNDORT
            TextEdit_Beschreibung.EditValue = oLost.KURZ

            If bShowAdressAuswahl Then
                AdressAuswahl.iAdressNr = oLost.ADRNR
            End If

            'Setzt die derzeitige ID ein
            iCurrentRecordID = iRecordID

            Return iRecordID
        Catch ex As Exception
            'Protokolliert den Fehler
            Proto.Error(ex)
            'Behandelt den Fehler
            OnError(ex)
            Return -1
        End Try
    End Function
";

            string sCodeAfter = @"
    ''' <summary>
    ''' (De-)Aktiviert die Bearbeitungs-Felder, sowie das Gegenteil f�r alle davon abh�ngigen Controlls
    ''' </summary>
    ''' <param name=""bEnable"">Bearbeitungs-Modus aktivieren</param>
    Private Sub Enable_EditControls(bEnable As Boolean) Implements IAsyncFilterableEditForm.Enable_EditControls
        Try

            'Wenn kein g�ltiger Datensatz ausgew�hlt ist und 
            'die Bearbeitungs-Felder aktiviert werden sollen
            If iCurrentRecordID < 0 AndAlso bEnable Then
                'Abbruch, da sonst nur Fehler
                Exit Sub
            End If

            classFunktionen.Edit_Data(bEnable, LayoutControl_Main)

        Catch ex As Exception
            'Protokolliert den Fehler
            Proto.Error(ex)
            'Behandelt den Fehler
            OnError(ex)
        End Try
    End Sub

    Private Async Function LoadAsync(iRecordID As Integer) As Task(Of Integer) Implements IAsyncFilterableEditForm.LoadAsync
        Try
            'Wenn keine g�ltige RecordID �bergeben wurde
            If iRecordID < 0 Then
                'Abbrechen
                Return -1
            End If

Await SqlFactory.LoadAsync(Of LOST)(New Query(""BESCHWER"").Where(NameOf(LOST.RECORDID), iRecordID)) = oLost

            'Daten einsetzten
If(oLost.DATUM Is Nothing, Nothing, classFunktionen.ToDateTime(CType(oLost.DATUM, Date), oLost.UHRZEIT)) = DateEdit_DateTime.EditValue
oLost.ITEM = TextEdit_Fundsache.EditValue
oLost.FUNDORT = TextEdit_Ort.EditValue
oLost.KURZ = TextEdit_Beschreibung.EditValue

            If bShowAdressAuswahl Then
oLost.ADRNR = AdressAuswahl.iAdressNr
            End If

            'Setzt die derzeitige ID ein
iRecordID = iCurrentRecordID

            Return iRecordID
        Catch ex As Exception
            'Protokolliert den Fehler
            Proto.Error(ex)
            'Behandelt den Fehler
            OnError(ex)
            Return -1
        End Try
    End Function
";

            Assert.Equal(sCodeAfter, await oCommand.Run(sCodeOriginal));
        }
    }
}
