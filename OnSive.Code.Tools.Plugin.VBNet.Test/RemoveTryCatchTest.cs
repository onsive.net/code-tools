using OnSive.Code.Tools.Common;
using OnSive.Code.Tools.TestExtension;
using System;
using Xunit;

namespace OnSive.Code.Tools.Plugin.VBNet.Test
{
    public class RemoveTryCatchTest : ICommandTest<RemoveTryCatch>
    {
        [Fact]
        public async void RunTest()
        {
            ICommand oCommand = new RemoveTryCatch();

            string sCodeOriginal = @"
    Private Sub SimpleButton_Verlassen_Click(sender As Object, e As EventArgs) Handles SimpleButton_Verlassen.Click
        Me.Close()
    End Sub

    Private Sub SimpleButton_Drucken_Click(sender As Object, e As EventArgs) Handles SimpleButton_Drucken.Click
        Try
            GridControl_Main.ShowRibbonPrintPreview()
        Catch ex As Exception
            'Protokolliert den Fehler
            Proto.Error(ex)
            'Behandelt den Fehler
            OnError(ex)
        End Try
    End Sub

    Private Sub SimpleButton_Drucken_Click(sender As Object, e As EventArgs) Handles SimpleButton_Drucken.Click
        Try
            'Test1
            'Test2
            'Test3
            GridControl_Main.ShowRibbonPrintPreview()
        Catch ex As Exception
            'Protokolliert den Fehler
            Proto.Error(ex)
            'Behandelt den Fehler
            OnError(ex)
        End Try
    End Sub

    Private Sub SimpleButton_OutlookExport_Click(sender As Object, e As EventArgs) Handles SimpleButton_OutlookExport.Click
        Try
            'Check ob nichts ausgew�hlt ist
            If iCurrentRecordID < 0 Then
                'Fehlermeldung anzeigen
                MessageBox.Show(GetText(Texte.Fehler_KeinDatensatzAusgewaehlt__0), GetText(Texte.Title__0), MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            'F�gt das TaskItem zu Outlook hinzu
            classOutlook.Add_TaskItem(MemoEdit_Beschreibung.Text.Split(vbNewLine)(0), MemoEdit_Beschreibung.Text, CheckEdit_Erledigt.Checked, DateEdit_DateTime.DateTime)
        Catch ex As Exception
            'Protokolliert den Fehler
            Proto.Error(ex)
            'Behandelt den Fehler
            OnError(ex)
        End Try
    End Sub
";

            string sCodeAfter = @"
    Private Sub SimpleButton_Verlassen_Click(sender As Object, e As EventArgs) Handles SimpleButton_Verlassen.Click
        Me.Close()
    End Sub

    Private Sub SimpleButton_Drucken_Click(sender As Object, e As EventArgs) Handles SimpleButton_Drucken.Click
            GridControl_Main.ShowRibbonPrintPreview()
    End Sub

    Private Sub SimpleButton_Drucken_Click(sender As Object, e As EventArgs) Handles SimpleButton_Drucken.Click
            'Test1
            'Test2
            'Test3
            GridControl_Main.ShowRibbonPrintPreview()
    End Sub

    Private Sub SimpleButton_OutlookExport_Click(sender As Object, e As EventArgs) Handles SimpleButton_OutlookExport.Click
        Try
            'Check ob nichts ausgew�hlt ist
            If iCurrentRecordID < 0 Then
                'Fehlermeldung anzeigen
                MessageBox.Show(GetText(Texte.Fehler_KeinDatensatzAusgewaehlt__0), GetText(Texte.Title__0), MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            'F�gt das TaskItem zu Outlook hinzu
            classOutlook.Add_TaskItem(MemoEdit_Beschreibung.Text.Split(vbNewLine)(0), MemoEdit_Beschreibung.Text, CheckEdit_Erledigt.Checked, DateEdit_DateTime.DateTime)
        Catch ex As Exception
            'Protokolliert den Fehler
            Proto.Error(ex)
            'Behandelt den Fehler
            OnError(ex)
        End Try
    End Sub

";

            Assert.Equal(sCodeAfter, await oCommand.Run(sCodeOriginal));
        }
    }
}
