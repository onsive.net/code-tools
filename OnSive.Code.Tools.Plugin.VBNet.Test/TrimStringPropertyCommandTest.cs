using OnSive.Code.Tools.Common;
using OnSive.Code.Tools.TestExtension;
using System;
using Xunit;

namespace OnSive.Code.Tools.Plugin.VBNet.Test
{
    public class TrimStringPropertyCommandTest : ICommandTest<TrimStringPropertyCommand>
    {
        [Fact]
        public async void RunTest()
        {
            ICommand oCommand = new TrimStringPropertyCommand();

            string sCodeOriginal = @"Namespace Models
    Public Class BESCHWER

        Private _RECORDID As Decimal
        Public Property RECORDID() As Decimal
            Get
                Return _RECORDID
            End Get
            Set(ByVal value As Decimal)
                _RECORDID = value
            End Set
        End Property

        Private _INDEX As Decimal
        Public Property INDEX() As Decimal
            Get
                Return _INDEX
            End Get
            Set(ByVal value As Decimal)
                _INDEX = value
            End Set
        End Property

        Private _DELETED As Decimal
        Public Property DELETED() As Decimal
            Get
                Return _DELETED
            End Get
            Set(ByVal value As Decimal)
                _DELETED = value
            End Set
        End Property

        Private _GASTNR As Decimal
        Public Property GASTNR() As Decimal
            Get
                Return _GASTNR
            End Get
            Set(ByVal value As Decimal)
                _GASTNR = value
            End Set
        End Property

        Private _DATUM As DateTime
        Public Property DATUM() As DateTime
            Get
                Return _DATUM
            End Get
            Set(ByVal value As DateTime)
                _DATUM = value
            End Set
        End Property

        Private _TIME As String
        Public Property TIME() As String
            Get
                Return _TIME
            End Get
            Set(ByVal value As String)
                _TIME = value
            End Set
        End Property

        Private _USER As String
        Public Property USER() As String
            Get
                Return _USER
            End Get
            Set(ByVal value As String)
                _USER = value
            End Set
        End Property

        Private _URSACHE As String
        Public Property URSACHE() As String
            Get
                Return _URSACHE
            End Get
            Set(ByVal value As String)
                _URSACHE = value
            End Set
        End Property

        Private _ABTEILUNG As Decimal
        Public Property ABTEILUNG() As Decimal
            Get
                Return _ABTEILUNG
            End Get
            Set(ByVal value As Decimal)
                _ABTEILUNG = value
            End Set
        End Property

        Private _STATUS As Decimal
        Public Property STATUS() As Decimal
            Get
                Return _STATUS
            End Get
            Set(ByVal value As Decimal)
                _STATUS = value
            End Set
        End Property

        Private _ZIMMERNR As String
        Public Property ZIMMERNR() As String
            Get
                Return _ZIMMERNR
            End Get
            Set(ByVal value As String)
                _ZIMMERNR = value
            End Set
        End Property

        Private _USERERL As String
        Public Property USERERL() As String
            Get
                Return _USERERL
            End Get
            Set(ByVal value As String)
                _USERERL = value
            End Set
        End Property

        Private _DATEERL As DateTime
        Public Property DATEERL() As DateTime
            Get
                Return _DATEERL
            End Get
            Set(ByVal value As DateTime)
                _DATEERL = value
            End Set
        End Property

        Private _TIMEERL As String
        Public Property TIMEERL() As String
            Get
                Return _TIMEERL
            End Get
            Set(ByVal value As String)
                _TIMEERL = value
            End Set
        End Property

        Private _GUID As Guid
        Public Property GUID() As Guid
            Get
                Return _GUID
            End Get
            Set(ByVal value As Guid)
                _GUID = value
            End Set
        End Property

        Private _SQLDELFLAG As Boolean
        Public Property SQLDELFLAG() As Boolean
            Get
                Return _SQLDELFLAG
            End Get
            Set(ByVal value As Boolean)
                _SQLDELFLAG = value
            End Set
        End Property

    End Class
End Namespace
";

            string sCodeAfter = @"Namespace Models
    Public Class BESCHWER

        Private _RECORDID As Decimal
        Public Property RECORDID() As Decimal
            Get
                Return _RECORDID
            End Get
            Set(ByVal value As Decimal)
                _RECORDID = value
            End Set
        End Property

        Private _INDEX As Decimal
        Public Property INDEX() As Decimal
            Get
                Return _INDEX
            End Get
            Set(ByVal value As Decimal)
                _INDEX = value
            End Set
        End Property

        Private _DELETED As Decimal
        Public Property DELETED() As Decimal
            Get
                Return _DELETED
            End Get
            Set(ByVal value As Decimal)
                _DELETED = value
            End Set
        End Property

        Private _GASTNR As Decimal
        Public Property GASTNR() As Decimal
            Get
                Return _GASTNR
            End Get
            Set(ByVal value As Decimal)
                _GASTNR = value
            End Set
        End Property

        Private _DATUM As DateTime
        Public Property DATUM() As DateTime
            Get
                Return _DATUM
            End Get
            Set(ByVal value As DateTime)
                _DATUM = value
            End Set
        End Property

        Private _TIME As String
        Public Property TIME() As String
            Get
                Return _TIME.Trim()
            End Get
            Set(ByVal value As String)
                _TIME = value
            End Set
        End Property

        Private _USER As String
        Public Property USER() As String
            Get
                Return _USER.Trim()
            End Get
            Set(ByVal value As String)
                _USER = value
            End Set
        End Property

        Private _URSACHE As String
        Public Property URSACHE() As String
            Get
                Return _URSACHE.Trim()
            End Get
            Set(ByVal value As String)
                _URSACHE = value
            End Set
        End Property

        Private _ABTEILUNG As Decimal
        Public Property ABTEILUNG() As Decimal
            Get
                Return _ABTEILUNG
            End Get
            Set(ByVal value As Decimal)
                _ABTEILUNG = value
            End Set
        End Property

        Private _STATUS As Decimal
        Public Property STATUS() As Decimal
            Get
                Return _STATUS
            End Get
            Set(ByVal value As Decimal)
                _STATUS = value
            End Set
        End Property

        Private _ZIMMERNR As String
        Public Property ZIMMERNR() As String
            Get
                Return _ZIMMERNR.Trim()
            End Get
            Set(ByVal value As String)
                _ZIMMERNR = value
            End Set
        End Property

        Private _USERERL As String
        Public Property USERERL() As String
            Get
                Return _USERERL.Trim()
            End Get
            Set(ByVal value As String)
                _USERERL = value
            End Set
        End Property

        Private _DATEERL As DateTime
        Public Property DATEERL() As DateTime
            Get
                Return _DATEERL
            End Get
            Set(ByVal value As DateTime)
                _DATEERL = value
            End Set
        End Property

        Private _TIMEERL As String
        Public Property TIMEERL() As String
            Get
                Return _TIMEERL.Trim()
            End Get
            Set(ByVal value As String)
                _TIMEERL = value
            End Set
        End Property

        Private _GUID As Guid
        Public Property GUID() As Guid
            Get
                Return _GUID
            End Get
            Set(ByVal value As Guid)
                _GUID = value
            End Set
        End Property

        Private _SQLDELFLAG As Boolean
        Public Property SQLDELFLAG() As Boolean
            Get
                Return _SQLDELFLAG
            End Get
            Set(ByVal value As Boolean)
                _SQLDELFLAG = value
            End Set
        End Property

    End Class
End Namespace
";

            Assert.Equal(sCodeAfter, await oCommand.Run(sCodeOriginal));
        }
    }
}
