﻿using OnSive.Code.Tools.Common;
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OnSive.Code.Tools.Plugin.VBNet
{
    public class RemoveTryCatch : ICommand
    {
        Regex oRegex = new Regex(@"\s*(Private|Public) (Async )?(Sub|Function) ([a-zA-Z0-9_]*)\(.*?\).*$");

        public string Title => "[VB] RemoveTryCatch";

        public string Description => "Removes all try-catchs on one row sub and functions.";

        public stReferences[] References => new stReferences[]
        {
        };

        public Task<string> Run(string sCode)
        {
            string[] sArrCode = sCode.Replace(Environment.NewLine, "\n").Split('\n');

            var sb = new StringBuilder();

            for (int i = 0; i < sArrCode.Length; i++)
            {
                var oMatch = oRegex.Match(sArrCode[i]);
                if ((sArrCode.Length > i + 9) && oMatch.Success && !string.IsNullOrEmpty(oMatch.Groups[4].Value))
                {
                    int iOffset = 0;
                    for (int x = i + 2; x < sArrCode.Length; x++)
                    {
                        if (sArrCode[x].Trim().StartsWith("'"))
                        {
                            iOffset++;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (sArrCode[i + 1].Contains("Try") && sArrCode[i + 3 + iOffset].Contains("Catch"))
                    {
                        sb.AppendLine(sArrCode[i]);

                        for (int y = i + 2; y < i + 2 + iOffset + 1; y++)
                        {
                            sb.AppendLine(sArrCode[y]);
                        }

                        i += 8 + iOffset;
                        continue;
                    }
                }
                sb.AppendLine(sArrCode[i]);
            }

            return Task.FromResult(sb.ToString());
        }
    }
}
