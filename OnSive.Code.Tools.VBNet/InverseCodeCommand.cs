﻿using OnSive.Code.Tools.Common;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OnSive.Code.Tools.Plugin.VBNet
{
    public class InverseCodeCommand : ICommand
    {
        Regex oRegex = new Regex(@"^\s*([a-zA-Z0-9_.]*?)?\s?([a-zA-Z0-9_.]+)\s*=\s*(.*?);?\s*$");

        public string Title => "Inverse Code (=)";

        public string Description => "Reverses the code on each line at a '='.";

        public stReferences[] References => new stReferences[]
        {
        };

        public Task<string> Run(string sCode)
        {
            string[] sArrCode = sCode.Replace(Environment.NewLine, "\n").Split('\n');


            for (int i = 0; i < sArrCode.Length; i++)
            {
                if (!sArrCode[i].Contains('='))
                {
                    continue;
                }

                var oMatch = oRegex.Match(sArrCode[i]);
                if (!oMatch.Success)
                {
                    continue;
                }

                string sDavor = oMatch.Groups[2].Value;
                string sDanach = oMatch.Groups[3].Value;

                sArrCode[i] = $"{sDanach} = {sDavor}";
            }

            return Task.FromResult(string.Join(Environment.NewLine, sArrCode));
        }
    }
}
