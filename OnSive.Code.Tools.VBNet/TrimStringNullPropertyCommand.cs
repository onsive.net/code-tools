﻿using OnSive.Code.Tools.Common;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OnSive.Code.Tools.Plugin.VBNet
{
    public class TrimStringNullPropertyCommand : ICommand
    {
        public string Title => "Trim String and nothing in Properties";

        public string Description => "Cuts of every leading and ending spaces of string as property which could be nothing, too.";

        public stReferences[] References => new stReferences[]
        {
        };

        public Task<string> Run(string sCode)
        {
            string[] sArrCode = sCode.Replace(Environment.NewLine, "\n").Split('\n');

            bool bStringProperty = false;
            bool bGetProperty = false;
            for (int i = 0; i < sArrCode.Length; i++)
            {
                if (!bStringProperty && sArrCode[i].Contains("() As String"))
                {
                    bStringProperty = true;
                }
                else if (!bGetProperty && sArrCode[i].Contains("Get"))
                {
                    bGetProperty = true;
                }
                else if (bStringProperty && bGetProperty)
                {
                    sArrCode[i] += "?.Trim()";
                    bStringProperty = false;
                    bGetProperty = false;
                }
                else
                {
                    bStringProperty = false;
                    bGetProperty = false;
                }
            }
            return Task.FromResult(string.Join(Environment.NewLine, sArrCode));
        }
    }
}
