﻿using OnSive.Code.Tools.Common;
using System;
using System.Linq;
using Xunit;

namespace OnSive.Code.Tools.TestExtension
{
    public class ICommandTest<T>
        where T : ICommand, new()
    {
        private T oCommand = new T();

        [Fact]
        public void TitleTest()
        {
            Assert.True(!string.IsNullOrWhiteSpace(oCommand.Title));
        }

        [Fact]
        public void DescriptionTest()
        {
            Assert.True(!string.IsNullOrWhiteSpace(oCommand.Description));
        }

        [Fact]
        public void ReferencesTest()
        {
            if (oCommand.References.Count() == 0)
            {
                Assert.True(true);
            }
            else
            {
                foreach (var oReference in oCommand.References)
                {
                    Assert.True(!string.IsNullOrWhiteSpace(oReference.AssemblyName));
                    Assert.True(!string.IsNullOrWhiteSpace(oReference.MinVersion.ToString()));
                }
            }
        }
    }
}
