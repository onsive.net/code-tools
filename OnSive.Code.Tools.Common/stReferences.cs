﻿using System;
using System.Linq;

namespace OnSive.Code.Tools.Common
{
    public struct stReferences
    {
        public string AssemblyName { get; private set; }

        public Version MinVersion { get; private set; }

        public stReferences(string AssemblyName, Version MinVersion)
        {
            this.AssemblyName = AssemblyName;
            this.MinVersion = MinVersion;
        }
    }
}
