﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace OnSive.Code.Tools.Common
{
    public interface ICommand
    {
        public string Title { get; }

        public string Description { get; }

        public stReferences[] References { get; }

        public Task<string> Run(string sCode);
    }
}
