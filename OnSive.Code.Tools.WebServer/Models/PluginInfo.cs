﻿using System;
using System.Linq;

namespace OnSive.Code.Tools.WebServer.Models
{
    public class PluginInfo
    {
        public string Name { get; private set; }

        public string[] Versions { get; private set; }

        public PluginInfo(string PluginName, string[] PluginVersions)
        {
            Name = PluginName;
            Versions = PluginVersions;
        }
    }
}
