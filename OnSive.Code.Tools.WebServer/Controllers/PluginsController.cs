﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OnSive.Code.Tools.WebServer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;

namespace OnSive.Code.Tools.WebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PluginsController : ControllerBase
    {
        private readonly ILogger<PluginsController> _logger;

        public PluginsController(ILogger<PluginsController> logger)
        {
            _logger = logger;
        }

        private static string GetPfad(string PluginName, string PluginVersion)
        {
            return Path.Combine(Environment.CurrentDirectory, "Data", "Plugins", PluginName, $"{PluginVersion}.dll");
        }

        [HttpGet("Info/{PluginName}")]
        public PluginInfo Info(string PluginName)
        {
            var diPlugin = new DirectoryInfo(Path.Combine(Environment.CurrentDirectory, "Data", "Plugins", PluginName));

            if (!diPlugin.Exists)
            {
                return null;
            }

            List<string> List_Versions = new List<string>();
            foreach (var fiVersions in diPlugin.GetFiles("*.dll", SearchOption.TopDirectoryOnly))
            {
                List_Versions.Add(fiVersions.Name.Replace(fiVersions.Extension, string.Empty));
            }
            return new PluginInfo(PluginName, List_Versions.OrderByDescending(x => x).ToArray());
        }

        [HttpGet("Download/{PluginName}/{PluginVersion}")]
        public FileResult Download(string PluginName, string PluginVersion)
        {
            string sFilePath = GetPfad(PluginName, PluginVersion);

            if (!System.IO.File.Exists(sFilePath))
            {
                return null;
            }

            byte[] byteArrFile = System.IO.File.ReadAllBytes(sFilePath);
            return File(byteArrFile, MediaTypeNames.Application.Octet, $"{PluginName}.dll");
        }
    }
}
